import React, { Component } from 'react'
import Button from '../Button/Button'
import './Modal.scss'

const Modal = ({header, iconCloseBtn, mainText, headerBackground, bodyBackground,  actions , open, close, key}) => {
    return(
        <div>
            { open ? <div className={open ? "modal-back-drop" : ""} onClick={close}></div> : null }

            <div className={open ? "modal active " + bodyBackground : "modal"}>
                <div className={"modal__header " + headerBackground}>
                    <h2 className="modal__title">{header}</h2>
                    <span 
                        className={iconCloseBtn ? "modal__close-btn active" : "modal__close-btn"}
                        onClick={close}
                    >
                        Х 
                    </span>
                </div>

                <div className="modal__body">
                    <p className="modal__main-text">{mainText}</p>

                    <div className="button-wrapper">
                        {actions}
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Modal;
