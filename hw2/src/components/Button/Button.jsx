import React from 'react';

const Button = ({textContent, onClickFunction, background}) => {
    return(
        <>
            <button 
                onClick={onClickFunction}
                style={{
                    background: background,
                }}
            >
                {textContent}
            </button>
        </>
    )
}

export default Button;