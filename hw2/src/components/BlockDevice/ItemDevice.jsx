import React, { useState, useEffect} from 'react';
import { FaStar, FaRegStar } from 'react-icons/fa';

import './BlockDevice.scss';

function ItemDevice({ phoneName, price, img, vendorCode, color, openModalAddToCart }) {
    const [favoritesBtn, setFavoritesBtn] = useState(false);

    const saveFavorites = () => {
        localStorage.setItem(`favorit ${vendorCode}`, true);
        setFavoritesBtn(true);
    }

    const deliteFavorite = () => {
        localStorage.removeItem(`favorit ${vendorCode}`);
        setFavoritesBtn(false);
    }

    const addShoppingCart = () => {
        localStorage.setItem(`shopping cart ${vendorCode}`, true)
    }

    useEffect(() => {
        const getItemFavorit = localStorage.getItem(`favorit ${vendorCode}`);
        getItemFavorit ? setFavoritesBtn(true) : setFavoritesBtn(false);
    },[])




    return (
        <li className="device__item">
            <img 
                src={img} 
                alt="device"
                className="device__item-img"
            />
            <div className="device__item-title">
                <span>{phoneName} </span>
                <span> {color}</span>
            </div>

            <p className="device-price">{price}</p>

            <div className="device__item-action">
                <button 
                    onClick={() => {
                        openModalAddToCart(); 
                        addShoppingCart();
                    }}>
                    
                    Добавить в корзину
                </button>


                {/* Fill icon  */} 
                <span 
                    className={`
                            icon device__item-action-icon
                            ${favoritesBtn ? "active" : ""}
                        `}
                    onClick={deliteFavorite}
                >
                    <FaStar/>
                </span>


                {/* Stroke icon  */}
                <span
                    className={`
                        icon device__item-action-icon 
                        ${favoritesBtn ? "" : "active"}
                    `}
                    onClick={saveFavorites}
                >
                     <FaRegStar />
                </span>
            </div>
        </li>
    )
}

export default ItemDevice;