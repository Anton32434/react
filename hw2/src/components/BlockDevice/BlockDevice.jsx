import React, { useState, useEffect } from 'react'
import ItemDevice from './ItemDevice'

import './BlockDevice.scss';
import Modal from '../Modal/Modal';



function BlockDevice() {
    const [devicesItem, setDevicesItem] = useState([]);
    const [openModalAddToCart, setOpenModalAddToCart] = useState(false);

    const getDevices = async () => {
        const res = await fetch("/device.json");
        const data = await res.json();
        setDevicesItem(() => data.iphone);
    }

    useEffect(() => {
        getDevices();
    },[])



    const closeModalAddToCart = () => {
        setOpenModalAddToCart(() => false);
    }

    const addModalToCart = () => {
        setOpenModalAddToCart(true);
    }

    return (
        <ul className="device__list">
            {
                devicesItem.map(item => (
                    <ItemDevice 
                        {...item} 
                        openModalAddToCart={addModalToCart}
                        key={item.vendorCode}
                    />
                ))
            }

            <Modal 
                mainText="Товар добавлен в корзину"
                iconCloseBtn="true"
                headerBackground="modal__header-yellow"
                bodyBackground="modal__yellow"
                open={openModalAddToCart}
                close={closeModalAddToCart}
                actions={[
                        <button onClick={closeModalAddToCart}>ОК</button>
                ]}
            />
        </ul>
    )
}

export default BlockDevice
