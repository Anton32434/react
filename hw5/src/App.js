import React from 'react';
import { BrowserRouter as Router, Switch, Route, Link  } from 'react-router-dom';

import './App.scss';

import BlockDevice from './components/BlockDevice/BlockDevice';
import Nav from './components/Nav/Nav';
import Cart from './page/Cart/Cart';
import Favorites from './page/Favorites/Favorites';
import Home from './page/Home/Home';





const App = () => {
  return(
        <div>
          <Router>
            <Nav />
            <Switch>
              <Route exact path="/" component={Home}/>
              <Route path="/favorites" component={Favorites}/>
              <Route path="/cart" component={Cart}/>
            </Switch>
          </Router>
        </div>
  )
}

export default App;
