import React from 'react';
import { BrowserRouter as Router, Link } from 'react-router-dom';

import './Nav.scss'

function Nav() {
    return (
        <nav className="nav">
            <ul className="nav__list">
                <li className="nav__item">
                    <Link className="nav__link" to="/">Home</Link>
                </li>
                <li className="nav__item">
                    <Link className="nav__link" to="/favorites">Favorite</Link>
                </li>
                <li className="nav__item">
                    <Link className="nav__link" to="/cart">Cart</Link>
                </li>
            </ul>  
        </nav>  
    )
}

export default Nav
