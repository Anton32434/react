import React, { useEffect } from 'react'
import ItemDevice from './ItemDevice'

import './BlockDevice.scss';
import Modal from '../Modal/Modal';
import { useDispatch, useSelector } from 'react-redux';
import { iphoneItemActions } from '../../store/iphoneItem';
import { modalActions } from '../../store/modal';



function BlockDevice() {
    const dispatch = useDispatch()

    const { iphoneItem, inCart, isFavorit } = useSelector(state => state.iphoneItems)
    const { modal } = useSelector(state => state.modal)
    
    useEffect(() => {
        dispatch(iphoneItemActions.getIphoneList());
    }, [])


    // Open / Close modal 
    const openModal = () => {
        dispatch(modalActions.openModal())
    }

    const closeModal = () => {
        dispatch(modalActions.closeModal())
    }



    return (
        <div>
            <ul className="device__list">
                {
                    iphoneItem.map(iphone => (
                        <ItemDevice 
                            {...iphone}
                            actionBtnText="Добавить в корзину"
                            openModalInItem={openModal}
                            isCart={inCart}
                            key={iphone.vendorCode}
                        />
                    ))
                }

                <Modal 
                    mainText="Товар добавлен в корзину"
                    iconCloseBtn="true"
                    headerBackground="modal__header-yellow"
                    bodyBackground="modal__yellow"
                    modalIsOpen={modal}
                    closeModal={closeModal}
                    actions={[
                            <button onClick={closeModal}>ОК</button>
                    ]}
                />

            </ul>
        </div>
    )
}

export default BlockDevice
