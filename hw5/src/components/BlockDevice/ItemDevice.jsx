import React, { useState, useEffect} from 'react';
import { FaStar, FaRegStar } from 'react-icons/fa';
import { useDispatch } from 'react-redux';
import { buyDeviceActions } from '../../store/buyDevice'

import './BlockDevice.scss';


function ItemDevice({ 
    phoneName, 
    price, 
    img, 
    vendorCode, 
    color, 
    openModalInItem, 
    actionBtnText, 
    isCart,
}) {
    const [favoritesBtn, setFavoritesBtn] = useState(false);
    const [checked, setChecked] = useState(true);
    const dispatch = useDispatch();

    // Delite / save to Favorites
    const saveFavorites = () => {
        localStorage.setItem(`favorite ${vendorCode}`, true);
        setFavoritesBtn(true);
    }

    const deliteFavorite = () => {
        localStorage.removeItem(`favorite ${vendorCode}`);
        setFavoritesBtn(false);
    }


    // Delite / add to Shopping Cart
    const addShoppingCart = () => {
        localStorage.setItem(`shopping cart ${vendorCode}`, true)
    }

    const deliteShoppingCart = () => {
        localStorage.removeItem(`shopping cart ${vendorCode}`);
    }


    const chooseProductToBuy = () => {
        console.log(checked);
        if (checked) {
            dispatch(buyDeviceActions.addPurchasedDevice(`shopping cart ${vendorCode}`))
            setChecked(() => false)
        } else {
            dispatch(buyDeviceActions.removePurchasedDevice(`shopping cart ${vendorCode}`))
            setChecked(() => true)
        }
    }


    useEffect(() => {
        const getItemFavorit = localStorage.getItem(`favorite ${vendorCode}`);
        getItemFavorit ? setFavoritesBtn(true) : setFavoritesBtn(false);
    },[])
    

    return (
        <li className="device__item">
            <img 
                src={img} 
                alt="device"
                className="device__item-img"
            />
            <div className="device__item-title">
                <span>{phoneName} </span>
                <span> {color}</span>
            </div>

            <p className="device-price">{price}</p>

            <div className="device__item-action">
                <button 
                    onClick={() => {
                        isCart ? deliteShoppingCart() : addShoppingCart();
                        openModalInItem();
                    }}>
                    
                    {actionBtnText}
                </button>


                {/* Fill icon  */} 
                <span 
                    className={`
                            icon device__item-action-icon
                            ${favoritesBtn ? "active" : ""}
                        `}
                    onClick={deliteFavorite}
                >
                    <FaStar/>
                </span>


                {/* Stroke icon  */}
                <span
                    className={`
                        icon device__item-action-icon 
                        ${favoritesBtn ? "" : "active"}
                    `}
                    onClick={saveFavorites}
                >
                     <FaRegStar />
                </span>
            </div>
        </li>
    )
}

export default ItemDevice;