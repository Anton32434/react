import './Form.scss';
import React, { useEffect } from 'react'
import { Formik } from 'formik';
import * as Yup from 'yup';
import { useDispatch, useSelector } from 'react-redux';
import { buyDeviceActions } from '../../store/buyDevice';

function Form({ deviceInCart }) {
    const formSchema = Yup.object().shape({
        firstName: Yup.string()
                    .min(3, 'Имя должно состоять более чем из 3 букв')
                    .typeError('Фамилия должена быть строкой')
                    .max(15, 'Имя должно составлять меньше 15 букв')
                    .required('Заполните поле'),
        lastName: Yup.string()
                    .min(3, 'Фамилия должна состоять более чем из 3 букв')
                    .typeError('Фамилия должена быть строкой')
                    .required('Заполните поле'),
        age: Yup.number()
                    .typeError('возраст должен быть числом')
                    .required('Заполните поле'),
        adress: Yup.string()
                    .required('Заполните поле'),
        mobileNumer: Yup.number()
                    .min(11, 'В номер телефона должно быть 11 цыфр')
                    .typeError('Номер должен быть числом')
                    .required('Заполните поле'),
    })

    const dispatch = useDispatch();
    const { devices, infoAboutСustomer } = useSelector(state => state.buyDevice);

    const buyDevices = (userData) => {
        const allDeviceInStorage = Object.keys(localStorage);
        const allPurchasedDevices = [];
        

        // Delite device in localStorage and dispatch device
        for (let keyLocalStorage of allDeviceInStorage) {
            for (let keydeviceInCart of deviceInCart) {
                if (keyLocalStorage === `shopping cart ${keydeviceInCart}`) {
                    allPurchasedDevices.push(keydeviceInCart);
                    dispatch(buyDeviceActions.addPurchasedDevice(allPurchasedDevices));
                    localStorage.removeItem(keyLocalStorage);
                }
            }
        } 

        dispatch(buyDeviceActions.getInfoAboutCustomer(userData));
    }



    useEffect(() => {
        console.log(devices);
    }, [devices])

    useEffect(() => {
        console.log(infoAboutСustomer);
    }, [infoAboutСustomer])




    return (
        <div className="container">
            <Formik
                initialValues={{
                    firstName: '',
                    lastName: '',
                    age: '',
                    adress: '',
                    mobileNumer: ''
                }}
                validationSchema={formSchema}
                onSubmit={(data) => buyDevices(data)}
            >
                {({
                    handleSubmit,
                    handleChange,
                    handleBlur,
                    errors,
                    touched
                }) => {
                    return (
                        <form onSubmit={handleSubmit}>
                            <div className="input-wrapper ">
                                <input 
                                    type="text"
                                    placeholder="Введите имя"
                                    name="firstName" 
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                />
                                {
                                    errors.firstName && 
                                    touched.firstName && 
                                    <p>{errors.firstName}</p>
                                }
                            </div>

                            <div className="input-wrapper ">
                                <input 
                                    type="text"
                                    placeholder="Введите фамилию"
                                    name="lastName" 
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                />
                                {
                                    errors.lastName && 
                                    touched.lastName && 
                                    <p>{errors.lastName}</p>
                                }
                            </div>
                            
                            <div className="input-wrapper ">
                                <input 
                                    type="number"
                                    placeholder="Введите возраст"
                                    name="age" 
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                />
                                {
                                    errors.age && 
                                    touched.age && 
                                    <p>{errors.age}</p>
                                }
                            </div>

                            <div className="input-wrapper ">
                                <input 
                                    type="text"
                                    placeholder="Введите адресс"
                                    name="adress" 
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                />
                                {
                                    errors.adress && 
                                    touched.adress && 
                                    <p>{errors.adress}</p>
                                }
                            </div>

                            <div className="input-wrapper ">
                                <input 
                                    type="tel"
                                    placeholder="Введите номер телефона"
                                    name="mobileNumer" 
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                />
                                {
                                    errors.mobileNumer && 
                                    touched.mobileNumer && 
                                    <p>{errors.mobileNumer}</p>
                                }
                            </div>

                            <button type="submit">Отправить</button>
                        </form>
                    )
                }}
            </Formik>
        </div>
    )
}

export default Form
