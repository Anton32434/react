import { buyDeviceActionsTypes } from "./buyDeviceActions";


const initialState = {
    devices: [],
    infoAboutСustomer: {}
}


export const buyDevice = (state = initialState, action) => {
    switch (action.type) {
        case buyDeviceActionsTypes.ADD_DEVICE_PURCHASED:
            return {
                ...state,
                devices: action.payload
            }
        
        case buyDeviceActionsTypes.INFO_ABOUT_CUSTOMER:
            return {
                ...state,
                infoAboutСustomer: action.payload
            }

        default:
            return state;
    }
}