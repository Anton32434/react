export const buyDeviceActionsTypes = {
    ADD_DEVICE_PURCHASED: "DEVICE_PURCHASED",
    INFO_ABOUT_CUSTOMER: "INFO_ABOUT_CUSTOMER"
}

export const buyDeviceActions = {
    addPurchasedDevice: (payload) => (
        {type: buyDeviceActionsTypes.ADD_DEVICE_PURCHASED, payload}
    ),

    getInfoAboutCustomer: (payload) => (
        {type: buyDeviceActionsTypes.INFO_ABOUT_CUSTOMER, payload}
    )
}