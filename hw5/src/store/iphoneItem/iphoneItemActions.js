export const iphoneItemActionsTypes = {
    FETCH_IPHONES_SUCCESS: "FETCH_IPHONES_SUCCESS",
    FETCH_IPHONES_ERR: "FETCH_IPHONES_ERR",
    IS_VAVORITE: "IS_VAVORITE"
}

export const iphoneItemActions = {
    getIphoneList: () => async (dispatch) => {
        try {
            const res = await fetch("/device.json");
            const json = await res.json()

            dispatch({type: iphoneItemActionsTypes.FETCH_IPHONES_SUCCESS, payload: json.iphone});
            dispatch({type: iphoneItemActionsTypes.FETCH_IPHONES_ERR, payload: false});
        } catch (err) {
            dispatch({type: iphoneItemActionsTypes.FETCH_IPHONES_ERR, payload: true})
        }
    },
    isFavorit: (payload) => (
        {type: iphoneItemActionsTypes.IS_VAVORITE, payload}
    )
}

