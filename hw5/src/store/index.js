import { applyMiddleware, combineReducers, compose, createStore } from 'redux'
import thunk from 'redux-thunk';
import { iphoneItems } from './iphoneItem';
import { modal } from './modal'
import { buyDevice } from './buyDevice'


const rootReducers = combineReducers({
    iphoneItems,
    modal,
    buyDevice
})

const store = createStore(rootReducers, compose(
    applyMiddleware(thunk),
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
));

export default store;
