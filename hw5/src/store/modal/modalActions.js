export const modalActionsTypes = {
    MODAL_OPEN: "MODAL_OPEN",
    CLOSE_MODAL: "CLOSE_MODAL"
}


export const modalActions = {
    openModal: () => (
        {type: modalActionsTypes.MODAL_OPEN, payload: true}
    ),

    closeModal: () => (
        {type: modalActionsTypes.CLOSE_MODAL, payload: false}
    )
}