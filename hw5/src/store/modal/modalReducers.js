import { modalActionsTypes } from "./modalActions";

const initialState = {
    modal: false
}

export const modal = (state = initialState, action) => {
    switch (action.type) {
        case modalActionsTypes.MODAL_OPEN:
            return {
                ...state,
                modal: action.payload
            }

        case modalActionsTypes.CLOSE_MODAL:
            return {
                ...state,
                modal: action.payload
            }

        default:
            return state;
    }   
}