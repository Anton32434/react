import React from 'react';
import './Button.scss';

const Button = ({textContent, onClickFunction, background}) => {
    return(
        <>
            <button 
                onClick={onClickFunction}
                style={{
                    background: background,
                }}
            >
                {textContent}
            </button>
        </>
    )
}

export default Button;