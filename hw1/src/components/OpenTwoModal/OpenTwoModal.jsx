import React, {useState} from 'react';
import Button from '../Button/Button';
import Modal from '../Modal/Modal';

const OpenTwoModal = () => {
    // Open Warning Modal;
    const [warningOpen, setWarningOpen] = useState(false);

    const openWarningModal = () => {
        setWarningOpen(true)
    }

    const closeWarningModal = () => {
        setWarningOpen(false)
    }


    // Open Confirm Modal 
    const [confirmOpen, setConfirmOpen] = useState(false);

    const openConfirmModal = () => {
        setConfirmOpen(true);
    }

    const closeConfirmModal = () => {
        setConfirmOpen(false);
    }

    return(
        <div>
            <Button
                textContent={"Open fist modal"}
                onClickFunction={openWarningModal}
            />
            <Button
                textContent={"Open second modal"}
                onClickFunction={openConfirmModal}
            />

            <Modal
                header={"Are you sure?"}
                mainText={"You will not be able to recover this imaginary file!"}
                iconCloseBtn={false}

                headerBackground="modal__header-red"
                bodyBackground="modal__red"

                open={warningOpen}
                close={closeWarningModal}

                actions={[
                    <Button
                        textContent="ok"
                        onClickFunction={closeWarningModal}
                    />,
                    <Button
                        textContent="close"
                        onClickFunction={closeWarningModal}
                    />
                ]}
            />

            <Modal
                header="Some title"
                mainText="Some subtitle!"
                iconCloseBtn={true}

                headerBackground="modal__header-red"
                bodyBackground="modal__green"

                open={confirmOpen}
                close={closeConfirmModal}

                actions={[
                    <Button
                        textContent="ok"
                        onClickFunction={closeConfirmModal}
                    />,
                    <Button
                        textContent="close"
                        onClickFunction={closeConfirmModal}
                    />
                ]}
            />
        </div>
    )
}

export default OpenTwoModal;