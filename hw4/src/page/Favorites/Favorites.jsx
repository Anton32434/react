import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import ItemDevice from '../../components/BlockDevice/ItemDevice';
import Modal from '../../components/Modal/Modal';
import { iphoneItemActions } from '../../store/iphoneItem';
import { modalActions } from '../../store/modal';


function Favorites() {
    const [favoritesIphones, setFavoritesIphones] = useState([]);

    const dispatch = useDispatch()

    const { iphoneItem, inCart } = useSelector(state => state.iphoneItems)
    const { modal } = useSelector(state => state.modal)
    
    useEffect(() => {
        dispatch(iphoneItemActions.getIphoneList());
    }, [])


    // Open / Close modal 
    const openModal = () => {
        dispatch(modalActions.openModal())
    }

    const closeModal = () => {
        dispatch(modalActions.closeModal())
    }

    const allDeviceInLocalStorage = Object.keys(localStorage);

    const getFavoriteDevice = () => {
        iphoneItem.forEach(iphone => {
            for (let vendorCode of allDeviceInLocalStorage) {
                if (vendorCode === `favorite ${iphone.vendorCode}`) {
                    setFavoritesIphones(prev => [...prev, iphone])
                }
            }
        });
    }



    useEffect(() => {
        getFavoriteDevice();
    }, [])

    return(
        <ul className="device__list">
            {
                favoritesIphones.map(iphone => (
                    <ItemDevice 
                        {...iphone}
                        actionBtnText="Добавить в корзину"
                        openModalInItem={openModal}
                        isCart={inCart}
                        key={iphone.vendorCode}
                    />
                ))
            }

            <Modal 
                mainText="Товар добавлен в корзину"
                iconCloseBtn="true"
                headerBackground="modal__header-yellow"
                bodyBackground="modal__yellow"
                modalIsOpen={modal}
                closeModal={closeModal}
                actions={[
                        <button onClick={closeModal}>ОК</button>
                ]}
            />
        </ul>
    )
}

export default Favorites;
