import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import ItemDevice from '../../components/BlockDevice/ItemDevice';
import Modal from '../../components/Modal/Modal';
import { iphoneItemActions } from '../../store/iphoneItem';
import { modalActions } from '../../store/modal';


function Cart() {
    const [iphoneInCart, setIphoneInCart] = useState([]);
    const dispatch = useDispatch()

    const { iphoneItem } = useSelector(state => state.iphoneItems)
    const { modal } = useSelector(state => state.modal)

    const getFavoriteDevice = async () => {        
        const allDeviceInLocalStorage = Object.keys(localStorage);

        iphoneItem.forEach(iphone => {
            for (let vendorCode of allDeviceInLocalStorage) {
                if (vendorCode === `shopping cart ${iphone.vendorCode}`) {
                    setIphoneInCart(prev => [...prev, iphone])
                }
            }
        });
    }


    // Open / Close modal 
    const openModal = () => {
        dispatch(modalActions.openModal())
    }

    const closeModal = () => {
        dispatch(modalActions.closeModal())
    }


    useEffect(() => {
        dispatch(iphoneItemActions.getIphoneList());
        getFavoriteDevice();
    }, [])
    

    return (
        <ul className="device__list">
            {
                iphoneInCart.map(iphone => (
                    <ItemDevice 
                        {...iphone}
                        actionBtnText="Удалить из корзины"
                        openModalInItem={openModal}
                        isCart={true}
                        key={iphone.vendorCode}
                    />
                ))
            }

            <Modal 
                mainText="Товар удален из корзины"
                iconCloseBtn="true"
                headerBackground="modal__header-yellow"
                bodyBackground="modal__yellow"
                modalIsOpen={modal}
                closeModal={closeModal}
                actions={[
                        <button onClick={closeModal}>ОК</button>
                ]}
            />
        </ul>
    )
}

export default Cart
