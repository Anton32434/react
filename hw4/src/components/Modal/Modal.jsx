import React from 'react'
import './Modal.scss'

const Modal = ({header, iconCloseBtn, mainText, headerBackground, bodyBackground,  actions , modalIsOpen, closeModal}) => {

    return(
        <div>
            { 
                modalIsOpen ? <div className={modalIsOpen ? "modal-back-drop" : ""} 
                onClick={closeModal}></div> : null 
            }
            <div className={modalIsOpen ? "modal active " + bodyBackground : "modal"}>
                <div className={"modal__header " + headerBackground}>
                    <h2 className="modal__title">{header}</h2>
                    <span 
                        className={iconCloseBtn ? "modal__close-btn active" : "modal__close-btn"}
                        onClick={closeModal}
                    >
                        Х 
                    </span>
                </div>

                <div className="modal__body">
                    <p className="modal__main-text">{mainText}</p>

                    <div className="button-wrapper">
                        {actions}
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Modal;
