import { iphoneItemActionsTypes } from "./iphoneItemActions";

const initialState = {
    iphoneItem: [],
    isFavorit: false,
    inCart: false,
    err: false
}

export const iphoneItems = (state = initialState, action) => {
    switch (action.type) {
        case iphoneItemActionsTypes.FETCH_IPHONES_SUCCESS:
            return {
                ...state,
                iphoneItem: action.payload
            }
        
        case iphoneItemActionsTypes.FETCH_IPHONES_ERR:
            return  {
                ...state,
                err: action.payload
            }

        default:
            return state;
    }
}