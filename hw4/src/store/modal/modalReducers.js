import { modalActionsTypes } from "./modalActions";

const initialState = {
    modal: false
}

export const modal = (state = initialState, action) => {
    switch (action.type) {
        case modalActionsTypes.MODAL_OPEN:
            console.log("open");
            return {
                ...state,
                modal: action.payload
            }

        case modalActionsTypes.CLOSE_MODAL:
            console.log("close");
            return {
                ...state,
                modal: action.payload
            }

        default:
            return state;
    }   
}