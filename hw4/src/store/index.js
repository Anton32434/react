import { applyMiddleware, combineReducers, compose, createStore } from 'redux'
import thunk from 'redux-thunk';
import { iphoneItems } from './iphoneItem';
import { modal } from './modal'


const rootReducers = combineReducers({
    iphoneItems,
    modal,
})

const store = createStore(rootReducers, compose(
    applyMiddleware(thunk),
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
));

export default store;
