import React, { useState, useEffect } from 'react'
import ItemDevice from './ItemDevice'

import './BlockDevice.scss';
import Modal from '../Modal/Modal';



function BlockDevice() {
    const [devicesItem, setDevicesItem] = useState([]);
    const [openModal, setOpenModal] = useState(false);

    const getDevices = async () => {
        const res = await fetch("/device.json");
        const data = await res.json();
        setDevicesItem(() => data.iphone);
    }

    useEffect(() => {
        getDevices();
    },[])

    

    const closeModal = () => {
        setOpenModal(false);
    }

    const openModalInItem = () => {
        setOpenModal(() => true);
    }

    return (
        <ul className="device__list">
            {
                devicesItem.map(item => (
                    <ItemDevice 
                        {...item} 
                        actionBtnText="Добавить в корзину"
                        isCart={false}
                        openModalInItem={openModalInItem}
                        key={item.vendorCode}
                    />
                ))
            }

            <Modal 
                mainText="Товар добавлен в корзину"
                iconCloseBtn="true"
                headerBackground="modal__header-yellow"
                bodyBackground="modal__yellow"
                open={openModal}
                close={closeModal}
                actions={[
                        <button onClick={closeModal}>ОК</button>
                ]}
            />
        </ul>
    )
}

export default BlockDevice
