import React, { useEffect, useState } from 'react'
import ItemDevice from '../../components/BlockDevice/ItemDevice';
import Modal from '../../components/Modal/Modal';

function Cart() {
    const [allDevice, setAllDevice] = useState([]);
    const [openModal, setOpenModal] = useState(false);

    const getFavoriteDevice = async () => {
        const res = await fetch("/device.json");
        const data = await res.json();
        
        const allDeviceInLocalStorage = Object.keys(localStorage);
        const allDevice = data.iphone;

        allDevice.forEach(device => {
            for (let vendorCode of allDeviceInLocalStorage) {
                if (vendorCode === `shopping cart ${device.vendorCode}`) {
                    setAllDevice(prev => [...prev, device])
                }
            }
        });
    }

    const closeModal = () => {
        setOpenModal(false);
    }

    const openModalInItem = () => {
        setOpenModal(() => true);
    }

    useEffect(() => {
        getFavoriteDevice()
    }, [])


    return (
        <ul className="device__list">
            {
                allDevice.map(item => (
                    <ItemDevice 
                        {...item} 
                        actionBtnText="Удалить с корзины"
                        isCart={true}
                        openModalInItem={openModalInItem}
                        key={item.vendorCode}
                    />
                ))
            }

            <Modal 
                mainText="Товар удален из корзины"
                iconCloseBtn="true"
                headerBackground="modal__header-yellow"
                bodyBackground="modal__yellow"
                open={openModal}
                close={closeModal}
                actions={[
                        <button onClick={closeModal}>ОК</button>
                ]}
            />
        </ul>
    )
}

export default Cart
