import React from 'react'
import BlockDevice from '../../components/BlockDevice/BlockDevice'


function Home() {
    return (
        <div>
            <BlockDevice />
        </div>
    )
}

export default Home
